# StabObject



### Introdution

StabObjects define how easy and deep a blade can stab into enemies' body 

### Some Important parameters you might want to adjust
#
Geos list: 

Multiply geometries can be added in order to cover all the edges of the blade.
![img](stabobject/multiple.jpg)


Defines where 

| Parameters  | Function       | 
| ----------- | ---------- |
| Geo point      | Defines where the center of Geo is    |
| Geo forward      | Defines where the edge points at. Note: use a normalized vector if possible | 
| Geo fwd dis        | How far you want to extend the edge      | 
| Thickness      | Namely, the length of it    | 
| Geo width       | The width     | 
| Stab Hand Vel Sqrt Require      | How fast you need to swing your weapon to trigger the stabbing    | 

Leave the rest parameters default until you gain more understandings to the mechanics.