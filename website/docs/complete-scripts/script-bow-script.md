---
title: BowScript
---

The bow script is part of the official ModProj project. It is used in the `Bow_Simple` weapon.
Checkout the code here: 
* [Bow Scripts](https://github.com/fonzieyang/BTModToolkit/tree/master/ModProj/Assets/Build/Bow_Simple/Script)