# Create a role mod



here is a step to make a simple role mod .

Note: The prefixes used in the tutorial are "WMD\_", which can be replaced with your own prefix, and you will need to change the prefix in the AddressableConfig(Assets/Resources/AddressableConfig) file. Once you have changed the prefix, you will need to fill in your new prefix instead of "WMD\_" in the tutorial.

#### 1.Open the ModProj by Unity2019.4.12f1



#### 2.Set the Animation Type to humanoid and Avatar Definition to Create From This Model

![](5.create-a-role-mod/model.png)



#### 3.Placement of the model into the scene

Create an Empty node under the left hand node, named "LWeapon Point", and do the same for the right hand node.

![](5.create-a-role-mod/weaponpoint.png)

Adjust the direction of the node, the z-axis direction of LWeapon Point is the direction of the weapon

![](5.create-a-role-mod/setweaponpoint.png)

Create two empty nodes under the Chest node, named "LWeapon Spine" and "RWeapon Spine",  This is the hanging point of the weapon, again requiring an angle adjustment.

![](5.create-a-role-mod/LWeaponSpine.png)

Apply.

![](5.create-a-role-mod/Apply.png)





#### 4.Save Model

Create these folder under Build folder.

![](4.create-a-skin-mod/folder.png)

Drag the character of the scene to the role folder.

![](5.create-a-role-mod/perfab.png)



#### 5.Take a screenshot

Select the Prefab and take a screenshot. Save the screenshot to ICon folder.

Set the Max size to 128 and click “Apply” to save.

![](5.create-a-role-mod/screenshot.png)



#### 6.Use ItemInfoConfig

1.Create an ItemInfoConfig file in the Config folder, name it with your mod name + "_Info”

![](5.create-a-role-mod/iteminfoconfig.png)

Fill in the RoleModInfo

![](5.create-a-role-mod/filliteminfoconfig.png)

**ReplaceRole**: Replaceable NPC in the game.

**Weapon**: character's weapon.

To know more about **ReplaceRole** and **Weapon**, please read [Role and Weapon](../details/role-and-weapon.md).



**attr**: character's attribute, read only the first data of this array, Use default data when array length is 0.

If you want to modify the sound effects of your character, you will need to place the sound files in the Audio folder.

![](5.create-a-role-mod/soundpath.png)

Fill in the sound file name in the config under SoundNames, and don't forget to add your prefixes.

![](5.create-a-role-mod/soundinfo.png)



### 7.Build

Select the **AddressbleConfig**(Assets - Resources - AddressableConfig), fill in the “Prefix” and “Addressable Paths”，Click ”**Clear Addressables**“ and “**Create And Refresh Addressable Name**”

![](5.create-a-role-mod/addressable.png)

Fill in the ProductName

![](5.create-a-role-mod/productname.png)

Click BuildTools - **BuildAllBundles** to Build.

![](5.create-a-role-mod/buildTool.png)



Done!